import Router from 'next/router'
import Head from 'next/head'
import Link from 'next/link'

import * as gtag from '../lib/gtag'

import '../styles/globals.scss'

Router.events.on('routeChangeComplete', (url) => gtag.pageview(url))

function MyApp({ Component, pageProps }) {
    return (
        <>
            <Head>
                <title>Pokémons</title>
            </Head>

            <section className="hero is-primary">
                <div className="hero-body">
                    <div className="container">
                        <h1 className="title">Pokémons</h1>
                    </div>
                </div>
                <div className="hero-foot">
                    <nav className="tabs">
                        <div className="container">
                            <ul>
                                <li className="is-active">
                                    <Link href="/">
                                        <a>Home</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/pikachu">
                                        <a>Pikachu</a>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </section>

            <section className="section">
                <div className="container">
                    <Component {...pageProps} />
                </div>
            </section>
        </>
    )
}

export default MyApp
