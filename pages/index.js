import getRandomPokemons from '../utils/randomPokemon'

import Pokegrid from '../components/Pokegrid'

export default function Home() {
    return <Pokegrid pokemons={getRandomPokemons(6)} />
}
