import { Fragment } from 'react'
import Document, { Head, Main, NextScript } from 'next/document'

import { GA_TRACKING_ID } from '../lib/gtag'

export default class CustomDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)

        // Check if in production
        const isProduction = process.env.NODE_ENV === 'production'

        return {
            ...initialProps,
            isProduction,
        }
    }

    render() {
        const { isProduction } = this.props

        return (
            <html lang="en">
                <Head>
                    <link rel="icon" href="/favicon.ico" />

                    {/* We only want to add the scripts if in production */}
                    {isProduction && (
                        <Fragment>
                            {/* Google Tag Manager */}
                            <script
                                dangerouslySetInnerHTML={{
                                    __html: `
                                        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                                        })(window,document,'script','dataLayer','GTM-P47RHH4');
                                    `,
                                }}
                            />
                            {/* End Google Tag Manager */}
                            {/* Global Site Tag (gtag.js) - Google Analytics */}
                            <script
                                async
                                src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
                            />
                            <script
                                dangerouslySetInnerHTML={{
                                    __html: `
                                        window.dataLayer = window.dataLayer || [];
                                        function gtag(){dataLayer.push(arguments);}
                                        gtag('js', new Date());

                                        gtag('config', '${GA_TRACKING_ID}', {
                                        page_path: window.location.pathname,
                                        });
                                    `,
                                }}
                            />
                        </Fragment>
                    )}
                </Head>
                <body>
                    {/* Google Tag Manager (noscript) */}
                    <noscript>
                        <iframe
                            src="https://www.googletagmanager.com/ns.html?id=GTM-P47RHH4"
                            height="0"
                            width="0"
                            style={{ display:'none', visibility:'hidden' }}
                        />
                    </noscript>
                    {/* End Google Tag Manager (noscript) */}
                    <Main />
                    <NextScript />
                </body>
            </html>
        )
    }
}
