import { useState, useEffect } from 'react'

import Head from 'next/head'
import Link from 'next/link'

import Pokecard from '../components/Pokecard'

const Pikachu = (props) => {
    const [variant, setVariant] = useState()
    const [interValId, setInterValId] = useState()

    useEffect(() => {
        async function getVariant() {
            if (window.dataLayer) {
                await window.dataLayer.push({ event: 'optimize.activate' })
            }
            const newIntervalId = setInterval(() => {
                if (window.google_optimize !== undefined) {
                    const variant = window.google_optimize.get(
                        'RYvKPEDvThWOfAom-C914w'
                    )
                    setVariant(variant)

                    if (interValId) clearInterval(interValId)
                }
            }, 100)

            setInterValId(newIntervalId)
        }

        getVariant()
    }, [])

    function getContent() {
        console.log(variant, typeof variant)
        if (variant === undefined) return <div>Loading...</div>
        if (variant === '1') {
            return (
                <Link href="/abtest">
                    <button className="button is-primary">Fim teste AB</button>
                </Link>
            )
        }

        return (
            <>
                <Pokecard pokemon={props.pikachu} />
                <Link href="/abtest">
                    <button className="button is-primary">Fim teste AB</button>
                </Link>
            </>
        )
    }

    return (
        <>
            <Head>
                <script src="https://www.googleoptimize.com/optimize.js?id=OPT-TG4RW8N"></script>
            </Head>
            {getContent()}
        </>
    )
}

export async function getStaticProps() {
    const res = await fetch('https://pokeapi.co/api/v2/pokemon/25/')
    const pikachu = await res.json()

    return {
        props: {
            pikachu,
        },
    }
}

export default Pikachu
