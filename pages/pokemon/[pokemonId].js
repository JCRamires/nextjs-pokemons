import { useRouter } from 'next/router'

import usePokemon from '../../hooks/usePokemon'

import Pokecard from '../../components/Pokecard'

function PokemonDetail() {
    const router = useRouter()
    const { pokemonId } = router.query
    const { pokemon, isLoading, isError } = usePokemon(pokemonId)

    if (isLoading) return <div>Loading ...</div>
    if (isError) return <div>Error ...</div>

    return <Pokecard pokemon={pokemon} />
}

export default PokemonDetail
