import useSWR from 'swr'

const fetcher = (...args) => fetch(...args).then((res) => res.json())

export default function usePokemon(pokemonId) {
    // FIXME: Este hook está fazendo um request undefined na página de detalhamento de pokemon
    const { data, error } = useSWR(
        `https://pokeapi.co/api/v2/pokemon/${pokemonId}/`,
        fetcher
    )

    return {
        pokemon: data,
        isLoading: !error && !data,
        isError: error,
    }
}
