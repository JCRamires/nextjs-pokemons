import Poketile from './Poketile'

function Pokegrid(props) {
    return (
        <>
            <h1 className="title">{props.pokemons.length} random pokemons</h1>
            <div className="tile is-ancestor" style={{ flexWrap: 'wrap' }}>
                {props.pokemons.map((pokemonId) => (
                    <Poketile
                        key={`pokemon-${pokemonId}`}
                        pokemonId={pokemonId}
                    />
                ))}
            </div>
        </>
    )
}

export default Pokegrid
