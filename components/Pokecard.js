import Link from 'next/link'
import cx from 'classnames'

import styles from '../styles/Pokecard.module.scss'

import { getPokemonArtwork } from '../utils/pokeUtils'

const Pokecard = (props) => (
    <Link href={`/pokemon/${props.pokemon.id}`}>
        <div className={cx('card', styles['pokemon-card'])}>
            <div className="card-image">
                <figure className="image is-square">
                    <img
                        src={getPokemonArtwork(props.pokemon)}
                        alt={`${props.pokemon.name} official artwork`}
                    />
                </figure>
            </div>
            <div className="card-content">
                <div className="media">
                    <div className="media-left">
                        <figure className="image is-64x64">
                            <img
                                src={props.pokemon.sprites.front_default}
                                alt={`${props.pokemon.name} sprite`}
                            />
                        </figure>
                    </div>
                    <div className="media-content">
                        <p className="title is-4">{props.pokemon.name}</p>
                        <div className="tags">
                            {props.pokemon.types.map((type) => (
                                <div
                                    key={`pokemon-${props.pokemon.id}-type-${type.type.name}`}
                                    className="tag"
                                >
                                    {type.type.name}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Link>
)

export default Pokecard
