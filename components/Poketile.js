import usePokemon from '../hooks/usePokemon'

import Pokecard from './Pokecard'

const getTile = (content) => <div className="tile is-4">{content}</div>

function Poketile(props) {
    const { pokemon, isLoading, isError } = usePokemon(props.pokemonId)

    if (isLoading) return getTile('Loading')
    if (isError) return getTile('Error')

    return <Pokecard pokemon={pokemon} />
}

export default Poketile
