export const getPokemonArtwork = (pokemon) =>
    pokemon.sprites.other['official-artwork'].front_default ||
    'https://via.placeholder.com/475?text=No+official+artwork'
