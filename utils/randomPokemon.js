const NUMBER_OF_POKEMONS = 890

const randomPokemonId = () => Math.floor(Math.random() * NUMBER_OF_POKEMONS) + 1

const getRandomPokemons = (numberOfPokemons = 1) => {
    const pokemons = []

    for (let index = 0; index < numberOfPokemons; index++) {
        pokemons.push(randomPokemonId())
    }

    return pokemons
}

export default getRandomPokemons
